# Python & Pandas interpolation

<p align="center">
    <a href="#description">Description</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="#installation">Installation</a>
</p>

## Description
This is a simple repository used for an interpolation test. I tried to interpolate missing values on a CSV file that uses semicolons as seperators.
The interpolation is achieved using Python and the Pandas libary. The code is commented out in the main.py so you can understand what it does if
you're currently learning how to do interpolations with Python and the Pandas libary.

## Installation
**Clone this repository**
```
$ git clone https://codeberg.org/braydofficial/pandas-interpolation.git
```
**Install the requirements using pip3**
```
$ cd pandas-interpolation
$ pip3 install -r requirements.txt
```
**Usage**
Just execute the .py-file
```
$ python3 main.py
```